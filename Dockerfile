ARG GO_IMAGE=golang:1.23
ARG WAL_G_VERSION
ARG PG_VERSION=17
ARG DEBIAN_VERSION=bookworm


FROM ${GO_IMAGE} AS wal-g-builder
ARG TARGETPLATFORM
ENV USE_LIBSODIUM=1
RUN --mount=type=cache,sharing=locked,target=/var/cache/apt \
    --mount=type=cache,sharing=locked,target=/var/lib/apt \
    apt-get update && \
    apt-get install -y --no-install-recommends libsodium-dev
RUN --mount=type=cache,target=/go/pkg/mod/ \
    --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/wal-g-clones \
    cd /wal-g-clones && \
    (git -C ${TARGETPLATFORM} pull || git clone https://github.com/wal-g/wal-g.git ${TARGETPLATFORM}) && \
    cd ${TARGETPLATFORM} && \
    git checkout ${WAL_G_VERSION} && git pull && \
    make deps pg_build && \
    mv main/pg/wal-g /bin/wal-g


FROM postgres:${PG_VERSION}-${DEBIAN_VERSION}
LABEL org.opencontainers.image.authors="Tony Gonçalves <tonyfg.pt@gmail.com>"
RUN --mount=type=cache,sharing=locked,target=/var/cache/apt \
    --mount=type=cache,sharing=locked,target=/var/lib/apt \
    apt-get update && \
    apt-get install -y ca-certificates && \
    apt-get clean
COPY --chown=root:root --from=wal-g-builder /bin/wal-g /usr/local/bin/wal-g
COPY --chown=root:root ./scripts/saasy-pg /usr/local/bin/saasy-pg
COPY --chown=root:root ./scripts/init-db-saasy-pg /docker-entrypoint-initdb.d/01-init-db-saasy-pg.sh
CMD ["saasy-pg"]
