.NOTPARALLEL:

IMG_TAG=pg${PG_VERSION}-walg${WAL_G_VERSION}

REPO_DIR := tmp/wal-g
PLATFORMS := linux/amd64,linux/arm,linux/arm64
WAL_G_VERSION := 3.0.5

all: pg14 pg14.17 pg14.16 \
     pg15 pg15.12 pg15.11 \
     pg16 pg16.8 pg16.7 \
     pg17 pg17.4 pg17.3

pg%:
	@docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
	@docker buildx create --use --name=crossplat --node=crossplat && \
	docker buildx build . \
		--pull \
		--push \
		--platform $(PLATFORMS) \
		-t tonyfg/saasy-pg:pg$(*)-walg$(WAL_G_VERSION) \
		--build-arg PG_VERSION=$(*) \
		--build-arg WAL_G_VERSION=v$(WAL_G_VERSION)
